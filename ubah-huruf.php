<?php

function ubah_huruf($string){
    $b ="";
    for( $i = 0; $i < strlen($string); $i++ ) {
        $b .= chr(ord($string[$i])-1);
    } 
      return $b;
    }
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";

echo ubah_huruf('keren'); // lfsfo
echo "<br>";

echo ubah_huruf('tfnbohbu!zb!sjp'); // tfnbohbu
echo "<br>";


?>